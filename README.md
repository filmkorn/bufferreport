# README #

Graphical, sortable representation of the current buffer usage

![buffer_report_screenshot.jpeg](https://bitbucket.org/repo/5pByLq/images/2085228874-buffer_report_screenshot.jpeg)

* Authors: Mitja Mueller-Jend
* License: MIT
* Installation:
    - Place the BufferReport folder inside your .nuke or PYTHONPATH
    - Add following lines to your menu.py:
```
menu = nuke.menu('Nuke').findItem("Cache")
menu.addCommand('Buffer Report', 'from BufferReport import buffer_table;br_widget = buffer_table.BufferReportWidget();br_widget.show()')
```
* If you want the BufferReport as dockable widget also add these lines:

```
from nukescripts import panels

def get_buffer_report_widget():
    # only import if needed and make the widget restorable from saved layout
    from BufferReport import buffer_table
    br_widget = buffer_table.BufferReportWidget()
    return  br_widget

pane = nuke.getPaneFor('Properties.1')
panels.registerWidgetAsPanel('get_buffer_report_widget', 'Buffer Report', 'de.filmkorn.BufferReport', True).addToPane(pane)
```